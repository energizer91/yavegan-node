/**
 * Created by xgmv84 on 11/26/2016.
 */

module.exports = function (configs) {
    var mongoose = require('mongoose'),
        mongoosastic = require('mongoosastic'),
        config = configs.mongo,
        db = mongoose.connection;

    mongoose.Promise = Promise;

    db.on('error', console.error);

    db.once('open', function () {
        console.log('MongoDB connection successful');
    });

    var userSchema = mongoose.Schema({
            username: String,
            first_name: String,
            last_name: String,
            user_id: Number,
            deleted: {type: Boolean, default: false},
            add_mode: {type: Boolean, default: false},
            feedback_mode: {type: Boolean, default: false},
            admin: {type: Boolean, default: false},
            editor: {type: Boolean, default: false},
            date: {type: Date, default: Date.now}
        }),
        quoteSchema = mongoose.Schema({
            author: {type: mongoose.Schema.Types.ObjectId, ref: userSchema},
            date: {type: Date, default: Date.now},
            text: {type: String, es_indexed: true}
        }),
        logSchema = mongoose.Schema({
            date: {
                type: Date,
                expires: 60 * 60 * 24 * 7,
                default: Date.now
            },
            request: Object,
            response: Object,
            error: Object
        }),
        statisticsSchema = mongoose.Schema({
            users: {
                count: Number,
                new: Number,
                deleted: Number
            },
            messages: {
                received: Number,
                popularCommand: String
            },
            date: {
                type: Date,
                expires: 60 * 60 * 24 * 365,
                default: Date.now
            }
        });

    quoteSchema.statics.random = function() {
        let request = {};
        return this.find(request).count().then(count => {
            let rand = Math.floor(Math.random() * count);
            return this.findOne(request).skip(rand).exec();
        });
    };

    quoteSchema.statics.convertId = function (id) {
        if (id) {
            return mongoose.Types.ObjectId(id);
        }
    };

    quoteSchema.statics.convertIds = function (ids) {
        if (Array.isArray(ids)) {
            return ids.map(function (id) {
                return mongoose.Types.ObjectId(id._id);
            })
        }

        return [];
    };

    mongoose.connect('mongodb://' + config.server + '/' + config.database);

    if (config.searchEngine === 'elastic') {
        quoteSchema.plugin(mongoosastic, {
            hosts: config.elasticHosts
        });
    } else if (config.searchEngine === 'native') {
        quoteSchema.index({text: "text"}, {weights: {content: 10, keywords: 5}, name: "text_text", default_language: "russian"});
    }


    return {
        Quote: mongoose.model('Quote', quoteSchema),
        User: mongoose.model('User', userSchema),
        Log: mongoose.model('Log', logSchema),
        Statistic: mongoose.model('Statistic', statisticsSchema)
    };
};