/**
 * Created by energizer on 30.06.17.
 */

module.exports = function (configs) {
    let requestApi = require('../helpers/request')(configs),
        botApi = require('../helpers/bot')(configs);
    return {
        broadcastQuotes: function (users, quotes, params, mongo) {
            let errorMessages = [];

            if (!users.length || !quotes.length) {
                return Promise.resolve([]);
            }

            return quotes.map(function (quote) {
                return botApi.sendMessageToAdmin('Start broadcasting message ' + JSON.stringify(quote), true).then(function () {
                    return requestApi.fulfillAll(users.map(function (user) {
                        return botApi.sendMessage(user.user_id, quote, params).catch(function (error) {
                            if (!error.ok && (error.error_code === 403) || (
                                error.description === 'Bad Request: chat not found' ||
                                error.description === 'Bad Request: group chat was migrated to a supergroup chat' ||
                                error.description === 'Bad Request: chat_id is empty')) {
                                errorMessages.push(user.user_id);
                                return {};
                            } else {
                                return botApi.sendMessageToAdmin('Sending message error: ' + JSON.stringify(error) + JSON.stringify(quote));
                            }
                        });
                    })).then(function () {
                        return botApi.sendMessageToAdmin('Broadcast finished', true).then(function () {
                            if (errorMessages.length) {
                                let text = errorMessages.length + ' messages has been sent with errors due to access errors. Unsubscribing them: \n' + errorMessages.join(', ');
                                console.log(text);
                                let bulk = mongo.User.collection.initializeOrderedBulkOp();
                                bulk.find({user_id: {$in: errorMessages}}).update({$set: {deleted: true}});
                                botApi.sendMessageToAdmin(text);
                                return bulk.execute();
                            }
                        })
                    });
                })
            });
            //return quotes.map(quote => users.map(user => botApi.sendMessage(user.user_id, quote)));
        }
    };
};