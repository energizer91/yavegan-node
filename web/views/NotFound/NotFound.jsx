import React from 'react';


const NotFound = () => (
    <h1>Sorry, the page you're looking for is unavaliable.</h1>
);

export default NotFound;