/**
 * Created by Александр on 13.12.2015.
 */
module.exports = function (express, botApi, configs) {
    var router = express.Router();

    var generateUserInfo = function (user) {
            return {
                text: '```\n' +
                'User ' + user.user_id + ':\n' +
                'Имя:        ' + (user.first_name    || 'Не указано') + '\n' +
                'Фамилия:    ' + (user.last_name     || 'Не указано') + '\n' +
                'Ник:        ' + (user.username      || 'Не указано') + '\n' +
                'Подписка:   ' + (user.subscribed     ? 'Подписан' : 'Не подписан') + '\n' +
                'Фидбэк:     ' + (user.feedback_mode  ? 'Включен'  : 'Выключен') + '\n' +
                'Админ:      ' + (user.admin          ? 'Присвоен' : 'Не присвоен') + '\n' +
                'Бан:        ' + (user.banned         ? 'Забанен'  : 'Не забанен') + '\n' +
                'Язык:       ' + (user.language      || 'Не выбран') + '\n' +
                'Клавиатура: ' + (user.keyboard       ? 'Включена' : 'Выключена') + '\n' +
                'Платформа:  ' + (user.client        || 'Не выбрана') + '```'
            };
        },
        generateStatistics = function (interval, stats) {
            return {
                text: '```\n' +
                'Статистика за ' + interval + ':\n' +
                'Пользователи\n' +
                'Всего:                  ' + stats.users.count + '\n' +
                'Новых:                  ' + stats.users.new + '\n' +
                'Удаленных:              ' + stats.users.deleted + '\n' +
                'Сообщения\n' +
                'Всего:                  ' + stats.messages.received + '```'
            };
        },
        commands = {
            '/quote': function (command, message, user) {
                if (command[1] === 'count') {
                    return botApi.mongo.Quote.count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'total_aneks_count', {aneks_count: count}), {language: user.language});
                    })
                } else if (command[1] === 'id') {
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate({language: user.language}, 'current_chat_id', {chat_id: message.chat.id}), {language: user.language});
                } else if (command[1] && (!isNaN(parseInt(command[1])))) {
                    return botApi.mongo.Quote.findOne().skip(parseInt(command[1]) - 1).exec().then(function (quote) {
                        return botApi.bot.sendMessage(message.chat.id, quote, {language: user.language});
                    }).catch(console.error);
                }
                return botApi.mongo.Quote.random().then(function (quote) {
                    return botApi.bot.sendMessage(message.chat.id, quote, {language: user.language, admin: user.admin && (message.chat.id === message.from.id)})
                })
            },
            '/message': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'broadcast_text_missing'));
                }

                command.splice(0, 1);

                var userId = command.splice(0, 1);

                return botApi.bot.sendMessage(userId, command.join(' '));

            },
            '/broadcast': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'broadcast_text_missing'));
                }

                command.splice(0, 1);

                return botApi.mongo.User.find({subscribed: true/*user_id: {$in: [85231140, 5630968, 226612010]}*/}).then(function (users) {
                    return botApi.request.fulfillAll(users.map(function (user) {
                        return this.sendMessage(user.user_id, command.join(' '));
                    }, botApi.bot));
                }).then(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Рассылка окончена.'));

            },
            '/grant': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, 'Введите id пользователя.');
                }

                var privileges = {};

                if (command[2]) {
                    if (command[2] === 'admin') {
                        privileges.admin = true;
                    } else if (command[2] === 'editor') {
                        privileges.editor = true;
                    }
                } else {
                    privileges.admin = false;
                    privileges.editor = false;
                }

                return botApi.mongo.User.findOneAndUpdate({user_id: parseInt(command[1])}, privileges).then(function () {
                    return botApi.bot.sendMessage(parseInt(command[1]), 'Вам были выданы привилегии администратора пользователем ' + user.first_name + '(' + user.username + ')');
                }).then(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Привилегии присвоены.'));
            },
            '/user': function (command, message, user) {
                if (command[1] === 'count') {
                    return botApi.mongo.User.count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_user_count', {count: count}));
                    })
                } else if (command[1] === 'subscribed') {
                    return botApi.mongo.User.find({subscribed: true}).count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_subscribed_user_count', {count: count}));
                    })
                } else if (command[1] === 'id') {
                    if (command[2]) {
                        return botApi.mongo.User.findOne({user_id: command[2]}).then(function (user) {
                            return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
                        })
                    }
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_user_id', {user_id: message.from.id}));
                }
                return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
            },
            '/quote_by_id': function (command, message, user) {
                return botApi.mongo.Quote.findOne({post_id: command[1]}).then(function (quote) {
                    return botApi.bot.sendMessage(message.chat.id, quote, {language: user.language});
                })
            },
            '/find_user': function (command, message) {
                return botApi.mongo.User.findOne({username: command[1]}).then(function (user) {
                    return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
                })
            },
            '/start': function (command, message, user) {
                if (command[1] && botApi.dict.languageExists(command[1])) {
                    user.language = command[1];
                } else if (command[1] && command[1] === 'donate') {
                    return botApi.bot.sendInvoice(message.from.id, {
                        title: 'Донат на развитие бота',
                        description: 'А то совсем нечего кушать',
                        payload: command[1]
                    })
                }
                
                return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'start'));
                });
            },
            '/help': function (command, message, user) {
                return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'start'));
            },
            '/chat': function (command, message) {
                if (!configs.bot.baneksLink) {
                    return botApi.bot.sendMessage(message.chat.id, 'денис дурак');
                }
                return botApi.bot.sendMessage(message.chat.id, 'Здесь весело: ' + configs.bot.baneksLink);
            },
            '/stat': function (command, message) {
                var startDate,
                    startTitle = 'всё время',
                    now = new Date();
                if (command[1]) {
                    if (command[1] === 'day') {
                        startDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                        startTitle = 'день';
                    } else if (command[1] === 'month') {
                        startDate = new Date(now.getFullYear(), now.getMonth());
                        startTitle = 'месяц';
                    } else if (command[1] === 'week') {
                        startDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() - (now.getDay() || 7) + 1);
                        startTitle = 'неделю';
                    } else {
                        startDate = new Date(now.getFullYear());
                    }
                } else {
                    startDate = new Date(now.getFullYear());
                }

                return botApi.statistics.getOverallStatistics(
                    botApi.mongo,
                    startDate,
                    new Date()
                ).then(function (results) {
                    return botApi.bot.sendMessage(message.chat.id, generateStatistics(startTitle, results), {disableButtons: true, parse_mode: 'Markdown'});
                })
            },
            '/add': function (command, message, user) {
                if (!(user.admin || user.editor)) {
                    throw new Error('Access denied.');
                }
                if (message && message.chat && message.from && (message.chat.id !== message.from.id)) {
                    return botApi.bot.sendMessage(message.chat.id, 'Комменты недоступны в группах.');
                }
                if (user.add_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Вы и так уже в режиме добавления.');
                } else {
                    user.add_mode = true;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим добавления включен.');
                    }).catch(function (error) {
                        return botApi.bot.sendMessage(user.user_id, 'Произошла ошибка: ' + error.message);
                    });
                }
            },
            '/feedback': function (command, message, user) {
                if (command[1] && user.admin) {
                    command.splice(0, 1);

                    var userId = command.splice(0, 1)[0];

                    return botApi.bot.sendMessage(userId, 'Сообщение от службы поддержки: ' + command.join(' '));
                } else if (user.feedback_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Вы и так уже в режиме обратной связи.');
                } else {
                    user.feedback_mode = true;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи включен. Вы можете писать сюда' +
                            ' любой текст (кроме команд) и он будет автоматически переведен в команду поддержки. Для остановки' +
                            ' режима поддержки отправьте /unfeedback');
                    });
                }
            },
            '/unfeedback': function (command, message, user) {
                if (command[1] && user.admin) {
                    command.splice(0, 1);

                    var userId = command.splice(0, 1)[0];

                    return botApi.mongo.User.findOneAndUpdate({user_id: userId}, {feedback_mode: false}).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим службы поддержки для пользователя ' + userId + ' отключен.');
                    });
                } else if (!user.feedback_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи и так отключен.');
                } else {
                    user.feedback_mode = false;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи отключен.');
                    });
                }
            }
        },
        performCommand = function (command, data, user) {
            return commands[command[0]].call(botApi.bot, command, data, user);
        },
        writeLog = function (data, result, error) {
            var logRecord = new botApi.mongo.Log({
                date: new Date(),
                request: data,
                response: result,
                error: error
            });

            return logRecord.save()
        },
        updateUser = function (user, callback) {
            if (!user) {
                return {};
            }

            return botApi.mongo.User.findOneAndUpdate({user_id: user.id}, user, {new: true, upsert: true, setDefaultsOnInsert: true}, callback);
        },
        searchQuotes = function (searchPhrase, limit, skip) {
            return botApi.mongo.Quote.find({$text: {$search: searchPhrase}}).limit(limit).skip(skip || 0).exec().then(function (results) {
                if (results.length) {
                    return results;
                }

                throw new Error('Nothing was found.');
            });
        },
        searchQuotesElastic = function (searchPhrase, limit, skip) {
            return new Promise(function (resolve, reject) {
                return botApi.mongo.Quote.esSearch({
                    from: skip,
                    size: limit,
                    query: {
                        query_string: {
                            query: searchPhrase
                        }
                    }
                }, function (err, results) {
                    if (err) {
                        return reject(err);
                    }

                    if (results && results.hits && results.hits.hits) {
                        return botApi.mongo.Quote.find({_id: {$in: botApi.mongo.Quote.convertIds(results.hits.hits)}}, function (err, quotes) {
                            if (err) {
                                return reject(err);
                            }

                            return resolve(quotes);
                        });
                    }

                    return reject(new Error('Nothing was found.'));
                });
            });
        },
        performSearch = function (searchPhrase, limit, skip) {
            if (configs.mongo.searchEngine === 'elastic') {
                return searchQuotesElastic(searchPhrase, limit, skip);
            }

            return searchQuotes(searchPhrase, limit, skip);
        },
        performInline = function (query, params) {
            var results = [],
                quotes_count = 5,
                searchAction;
            if (!params) {
                params = {};
            }
            if (!query.query) {
                searchAction = botApi.mongo.Quote.find({text: {$ne: ''}})
                    .sort({date: -1})
                    .skip(query.offset || 0)
                    .limit(quotes_count)
                    .exec();
            } else {
                searchAction = performSearch(query.query, quotes_count, query.offset || 0);
            }
            return searchAction.then(function (quotes) {
                results = quotes.map(function (quote, index) {
                    return {
                        type: 'article',
                        id: (index + 1).toString(),
                        title: botApi.dict.translate(params.language, 'anek_number', {number: (index + 1) || 0}),
                        input_message_content: {
                            message_text: quote.text,
                            parse_mode: 'HTML'
                        },
                        //message_text: quote.text,
                        description: quote.text.slice(0, 100)
                    };
                });

                return botApi.bot.sendInline(query.id, results, query.offset + quotes_count);
            }).catch(function () {
                return botApi.bot.sendInline(query.id, results, query.offset + quotes_count);
            });
        },
        acceptSuggest = function (queryData, data, params, anonymous) {
            return botApi.mongo.Suggest.findOneAndUpdate({_id: botApi.mongo.Suggest.convertId(queryData[1])}, {approved: true}).then(function (suggest) {
                return botApi.bot.answerCallbackQuery(data.callback_query.id)
                    .then(botApi.bot.editMessageButtons.bind(botApi.bot, data.callback_query.message, []))
                    .then(botApi.bot.forwardMessageToChannel.bind(botApi.bot, suggest, {native: anonymous}))
                    .then(function (sendMessage) {
                        return botApi.bot.sendMessage(data.callback_query.message.chat.id, 'Предложение одобрено.').then(function () {
                            return botApi.mongo.User.findOne({_id: suggest.user}).then(function (foundUser) {
                                if (sendMessage.ok && sendMessage.result) {
                                    return botApi.bot.forwardMessage(foundUser.user_id, sendMessage.result, {native: true});
                                }
                            });
                        });
                    });
            });
        },
        performCallbackQuery = function (queryData, data, params) {
            if (!params) {
                params = {};
            }
            switch (queryData[0]) {
                case 'spam':
                    return botApi.mongo.Quote.findOneAndUpdate({post_id: queryData[1]}, {spam: true}).then(function () {
                        return botApi.bot.answerCallbackQuery(data.callback_query.id).then(function () {
                            return botApi.bot.sendMessage(data.callback_query.message.chat.id, 'Анек помечен как спам.');
                        });
                    });
                case 'unspam':
                    return botApi.mongo.Quote.findOneAndUpdate({post_id: queryData[1]}, {spam: false}).then(function () {
                        return botApi.bot.answerCallbackQuery(data.callback_query.id).then(function () {
                            return botApi.bot.sendMessage(data.callback_query.message.chat.id, 'Анек помечен как нормальный.');
                        });
                    });
                case 's_a':
                    return acceptSuggest(queryData, data, params, true);
                case 's_aa':
                    return acceptSuggest(queryData, data, params, false);
                case 's_d':
                    return botApi.mongo.Suggest.findOneAndRemove({_id: botApi.mongo.Suggest.convertId(queryData[1])})
                        .then(botApi.bot.answerCallbackQuery.bind(botApi.bot, data.callback_query.id))
                        .then(botApi.bot.editMessageButtons.bind(botApi.bot, data.callback_query.message, []));
                case 's_da':
                    return botApi.mongo.Suggest.findOneAndUpdate({_id: botApi.mongo.Suggest.convertId(queryData[1])}, {public: true}).then(function () {
                        return botApi.bot.answerCallbackQuery(data.callback_query.id)
                            .then(botApi.bot.editMessageButtons.bind(botApi.bot, data.callback_query.message, []))
                            .then(botApi.bot.sendMessage.bind(botApi.bot, data.callback_query.message.chat.id, 'Предложение будет опубликовано неанонимно.'));
                    });
                case 'a_a':
                    return botApi.mongo.Quote.findOneAndUpdate({_id: botApi.mongo.Quote.convertId(queryData[1])}, {})
            }

            throw new Error('Unknown callback query ' + queryData);
        },
        performWebHook = function (data, response) {
            return new Promise(function (resolve, reject) {

                response.status(200);
                response.json({status: 'OK'});

                if (!data) {
                    return reject(new Error('No webhook data specified'));
                }

                if (data.hasOwnProperty('pre_checkout_query')) {
                    return resolve({});
                }

                var userObject = data.message || data.inline_query || data.callback_query;

                updateUser((userObject || {}).from, function (err, user) {
                    if (err) {
                        console.error(err);
                        return resolve({});
                    }
                    return resolve(user);
                });
            }).then(function (user) {
                console.log('Performing message from ' + botApi.bot.getUserInfo(user));

                if (data.hasOwnProperty('callback_query')) {
                    var queryData = data.callback_query.data.split(' ');
                    return performCallbackQuery(queryData, data, {language: user.language});
                } else if (data.hasOwnProperty('inline_query')) {
                    return performInline(data.inline_query, {language: user.language});
                } else if (data.message) {
                    var message = data.message;

                    if (message.new_chat_member) {
                        return botApi.bot.sendMessage(message.chat.id, 'Я веган.');
                    } else if (message.new_chat_member) {
                        return botApi.bot.sendMessage(message.chat.id, 'Пошёл нахуй, мясоед.');
                    } if (user.add_mode) {
                        var quote = {
                            author: user,
                            text: message.text
                        };

                        return new botApi.mongo.Quote(quote).save().then(function (newQuote) {
                            user.add_mode = false;
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                                return botApi.bot.sendMessage(user.user_id, {text: 'Цитата успешно добавлена.'});
                            });
                        });
                    } else if (message.text) {
                        var command = (message.text || '').split(' ');
                        if (command[0].indexOf('@') >= 0) {
                            command[0] = command[0].split('@')[0];
                        }

                        if (commands[command[0]]) {
                            return performCommand(command, message, user);
                        } else {
                            if (user.feedback_mode && !user.banned) {
                                return botApi.bot.forwardMessage(configs.bot.adminChat, message, {native: true});
                            }
                            console.error('Unknown command', data);
                            throw new Error('Command not found: ' + command.join(' '));
                        }
                    }
                }
                console.error('unhandled message', data);
                throw new Error('No message specified');
            }).then(function (response) {
                return writeLog(data, response).then(function () {
                    return response;
                })
            }).catch(function (error) {
                console.error(error);
                return writeLog(data, {}, error).then(function () {
                    return error;
                })
            });
        };

    router.get('/', function (req, res) {
        return res.send('hello fot Telegram bot api');
    });

    router.get('/getMe', function (req, res, next) {
        return botApi.bot.getMe().then(function (response) {
            return res.send(JSON.stringify(response));
        }).catch(next);
    });

    router.get('/grant', function (req, res) {
        if (!req.query.secret || (req.query.secret !== configs.bot.secret)) {
            return res.send('Unauthorized')
        }

        return botApi.mongo.User.findOneAndUpdate({user_id: 5630968}, {admin: true}).then(function () {
            return res.send('ok');
        });
    });

    router.route('/webhook')
        .post(function (req, res) {
            return performWebHook(req.body, res);
        });

    return {
        endPoint: '/bot',
        router: router
    };
};