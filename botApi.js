/**
 * Created by Алекс on 29.11.2016.
 */

module.exports = function (configs) {
    return {
        bot: require('./helpers/bot')(configs),
        mongo: require('./helpers/mongo')(configs),
        request: require('./helpers/request')(configs),
        dict: require('./helpers/dictionary'),
        statistics: require('./helpers/statistics')(configs)
    }
};